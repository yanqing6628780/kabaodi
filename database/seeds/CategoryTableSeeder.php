<?php

use Illuminate\Database\Seeder;
use App\Model\Category;

class CategoryTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('category')->delete();

		// Earing
		Category::create(array(
				'cateName' => '耳环',
				'ordering' => '0',
				'status' => 'Enable'
			));

		// Pendant
		Category::create(array(
				'cateName' => '吊坠',
				'ordering' => 1,
				'status' => 'Enable'
			));

		// Necklace 
		Category::create(array(
				'cateName' => '项链',
				'ordering' => 2,
				'status' => 'Enable'
			));

		// Ring
		Category::create(array(
				'cateName' => '戒指',
				'ordering' => 3,
				'status' => 'Enable'
			));

		// Bracelet
		Category::create(array(
				'cateName' => '手链',
				'ordering' => 4,
				'status' => 'Enable'
			));

		// Package
		Category::create(array(
				'cateName' => '套件',
				'ordering' => 5,
				'status' => 'Enable'
			));
	}
}