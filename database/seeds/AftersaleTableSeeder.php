<?php

use Illuminate\Database\Seeder;
use App\Model\Aftersale;

class AftersaleTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('aftersale')->delete();

		// Repair
		Aftersale::create(array(
				'memberId' => 1,
				'prove' => '23542364523',
				'reason' => '需要維修',
				'asDesc' => '詳細內容',
				'asImg' => 'repair.jpg'
			));
	}
}