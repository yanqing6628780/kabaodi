<?php

use Illuminate\Database\Seeder;
use App\Model\Member;

class MemberTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('member')->delete();

		// kabaodi01
		Member::create(array(
				'memberName' => 'kabaodi01',
				'memberPwd' => Hash::make('123456'),
				'mobile' => '1370000001',
				'email' => 'kabaodi01@kabaodi.com',
				'profile' => 'test.jpg',
				'status' => 'Enable'
			));

		// kabaodi02
		Member::create(array(
				'memberName' => 'kabaodi02',
				'memberPwd' => Hash::make('123456'),
				'mobile' => '1370000002',
				'email' => 'kabaodi02@kabaodi.com',
				'profile' => 'test.jpg',
				'status' => 'Enable'
			));

		// kabaodi03
		Member::create(array(
				'memberName' => 'kabaodi03',
				'memberPwd' => Hash::make('123456'),
				'mobile' => '1370000003',
				'email' => 'kabaodi03@kabaodi.com',
				'profile' => 'test.jpg',
				'status' => 'Enable'
			));
	}
}