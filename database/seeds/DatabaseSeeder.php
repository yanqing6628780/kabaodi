<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Model::unguard();

		$this->call('MemberTableSeeder');
		$this->command->info('Member table seeded!');

		$this->call('CategoryTableSeeder');
		$this->command->info('Category table seeded!');

		$this->call('AftersaleTableSeeder');
		$this->command->info('Aftersale table seeded!');

		$this->call('NewsTableSeeder');
		$this->command->info('News table seeded!');
	}
}