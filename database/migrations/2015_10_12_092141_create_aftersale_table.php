<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAftersaleTable extends Migration {

    public function up()
    {
        Schema::create('aftersale', function(Blueprint $table) {
            $table->increments('asId');
            $table->integer('memberId')->unsigned();
            $table->string('prove', 100);
            $table->string('reason', 255);
            $table->text('asDesc');
            $table->string('asImg', 100);
            $table->tinyInteger('status')->default(0);//0:未处理 1:已处理
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aftersale');
    }
}