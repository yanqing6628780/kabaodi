<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryTable extends Migration {

	public function up()
	{
		Schema::create('category', function(Blueprint $table) {
			$table->increments('cateId');
			$table->string('cateName', 100);
			$table->tinyInteger('ordering');
			$table->enum('status', array('Enable', 'Disable'));
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('category');
	}
}