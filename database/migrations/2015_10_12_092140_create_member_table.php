<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberTable extends Migration {

    public function up()
    {
        Schema::create('member', function(Blueprint $table) {
            $table->increments('memberId');
            $table->string('memberName', 100);
            $table->string('memberPwd', 100);
            $table->string('mobile', 100);
            $table->string('email', 100);
            $table->string('name', 100);
            $table->string('wechat', 100);
            $table->string('profile', 100);
            $table->enum('status', array('Enable', 'Disable'));
            $table->timestamps();
        });
        
        Schema::create('memberpwd_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::drop('member');
        Schema::drop('memberpwd_resets');
    }
}