<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

    public function up()
    {
        Schema::create('news', function(Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->default(1); //1:新闻活动 2:行业资讯
            $table->string('title', 128);
            $table->string('img', 255);
            $table->text('desc');
            $table->text('content');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('news');
    }
}