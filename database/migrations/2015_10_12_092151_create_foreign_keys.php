<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('product', function(Blueprint $table) {
			$table->foreign('cateId')->references('cateId')->on('category')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('aftersale', function(Blueprint $table) {
			$table->foreign('memberId')->references('memberId')->on('member')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('product', function(Blueprint $table) {
			$table->dropForeign('product_cateId_foreign');
		});
		Schema::table('aftersale', function(Blueprint $table) {
			$table->dropForeign('aftersale_memberId_foreign');
		});
	}
}