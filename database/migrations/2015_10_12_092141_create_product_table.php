<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	public function up()
	{
		Schema::create('product', function(Blueprint $table) {
			$table->increments('productId');
			$table->integer('cateId')->unsigned();
			$table->string('productName', 100);
			$table->text('productDesc');
			$table->string('productImg', 100);
			$table->text('gallery');
			$table->enum('status', array('Enable', 'Disable'));
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('product');
	}
}