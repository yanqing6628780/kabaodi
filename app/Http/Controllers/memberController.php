<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as Controller;
use App\Model\Member;
use App\Model\Memberpwd_resets as Mpwdresets;
use App\Model\Aftersale;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Mail;

class memberController extends Controller
{
    public function __construct()
    {
        
    }

    public function getCaptcha()
    {
        return captcha_src();
    }

    public function getLogin()
    {
        return view("memberLogin");
    }

    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'userPwd' => 'required',
            'captcha' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('message', '验证码错误');
        }else{
            $member = Member::where('memberName', $request->input('username'))->first();
            if($member && $member->status == 'Disable'){
                return redirect()->back()->withInput()->with('message', '会员帐户未激活或已被禁用。如果未激活，请到注册时所填邮箱查收激活邮件。');
            }
            if($member && Hash::check($request->input('userPwd'), $member->memberPwd)){
                $response['success'] = TRUE;
                $request->session()->put('isLogin', TRUE);
                $request->session()->put('member', $member->memberId);
                return redirect('member/profile');
            }else{
                return redirect()->back()->withInput()->with('message', '用户名或密码错误');
            }
        }
    }

    public function getLogout(Request $request)
    {
        $request->session()->put('isLogin', FALSE);
        $request->session()->put('member', null);
        return redirect('/home');
    }

    public function getRegister(Request $request)
    {
        if($request->session()->get('isLogin', FALSE)){        
            return redirect('member/profile');
        }else{
            return view('memberRegister');
        }
    }

    public function postRegister(Request $request)
    {
        $messages = [
            'username.unique' => '该用户名已注册',
            'email.unique' => '该邮箱已注册',
            'userPwd.confirmed' => '两次输入的密码不一致',
            'userPwd.between' => '密码必须是6到16位',
            'captcha.captcha' => '验证码错误',
        ];
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255|unique:member,memberName',
            'userPwd' => 'required|confirmed|between:6,16',
            'email' => 'required|email|max:255|unique:member',
            'name' => 'required',
            'captcha' => 'required|captcha',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $data = ['email'=>$request->input('email')];
            Mail::send('memberActive', $data, function($message) use($data)
            {
                $subject = "[卡宝地] 会员激活";
                $name = "顾客";
                $message->to($data['email'], $name)->subject($subject);
            });
            $member = new Member;
            $member->memberName = $request->input('username');
            $member->memberPwd = Hash::make($request->input('userPwd'));
            $member->email = $request->input('email');
            $member->name = $request->input('name');
            $member->mobile = $request->input('mobile');
            $member->status = 'Disable';
            $member->save();
            $request->session()->put('isLogin', FALSE);
            $request->session()->put('member', null);
            return redirect('member/profile');
        }
    }

    public function getActive(Request $request)
    {
        $messages = [
            'email.required' => '请填写邮箱',
            'email.exists' => '邮箱不存在',
            'email.email' => '请输入有效邮箱',
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:member,email'
        ],$messages);

        if ($validator->fails()) {
            return "非法请求";
        }else{
            $m = Member::where('email', $request->input('email'))->first();
            if($m){            
                $m->status = "Enable";
                $m->save();
                $request->session()->put('isLogin', TRUE);
                $request->session()->put('member', $m->memberId);
                return redirect('member/profile');
            }else{
                return "会员帐户不存在.<a href='".url('member/register')."'>注册</a> ";
            }
        }
    }

    public function getProfile(Request $request)
    {
        if($request->session()->get('isLogin', FALSE)){        
            $data['member'] = Member::find($request->session()->get('member'));
            return view('memberProfile', $data);
        }else{
            $request->session()->put('member', null);
            return redirect('member/login');
        }
    }

    public function postProfile(Request $request)
    {
        if(!$request->session()->get('isLogin', FALSE)){
            return redirect('member/login');
        }

        $member = Member::find($request->session()->get('member'));
        if($request->input('password')){
            $member->memberPwd = Hash::make($request->input('password'));
        }
        $member->email = $request->input('email');
        $member->name = $request->input('name');
        $member->mobile = $request->input('mobile');
        $profile = $this->upload_file('profile', $request);
        if($profile){
            @unlink(public_path($member->profile));
            $member->profile = $profile;
        }
        $member->save();
        return redirect()->back()->with('message', '提交成功');
    }

    public function postAftersale(Request $request)
    {
        if(!$request->session()->get('isLogin', FALSE)){
            return redirect('member/login');
        }

        $validator = Validator::make($request->all(), [
            'prove' => 'required',
            'reason' => 'required',
            'asDesc' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('message', '请填写完整所有信息');
        }else{
            $Aftersale = new Aftersale;
            $Aftersale->memberId = $request->session()->get('member');
            $Aftersale->prove = $request->input('prove');
            $Aftersale->reason = $request->input('reason');
            $Aftersale->asDesc = $request->input('asDesc');
            $Aftersale->asImg = $this->upload_file('asImg', $request);
            $Aftersale->save();
            return redirect()->back()->with('message', '您的售后申请已提交成功，我们将尽快电话联络您预约售后服务时间。感谢您的信赖！');
        }
    }

    public function getLookforpwd(Request $request)
    {
        return view('memberlookforpwd');
    }

    public function postLookforpwd(Request $request)
    {
        $messages = [
            'email.required' => '请填写邮箱',
            'email.exists' => '邮箱不存在',
            'email.email' => '请输入有效邮箱',
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:member,email'
        ],$messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }else{
            $input_data = $request->only('email');
            $m = Member::where('email', $input_data['email'])->first();

            $token = $this->createNewToken();
            Mpwdresets::where('email', $input_data['email'])->delete();
            Mpwdresets::create([
                'email' => $input_data['email'],
                'token' => $token
            ]);
            $data = ['email'=>$input_data['email'], 'token'=>$token];
            Mail::send('forgetpwdmail', $data, function($message) use($data)
            {
                $subject = "[卡宝地] 密码重设";
                $name = "顾客";
                $message->to($data['email'], $name)->subject($subject);
            });

            return redirect()->back()->with('message', '提交成功');
        }
    }

    public function getResetpwd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'token' => 'required',
        ]);
        $data['status'] = 0;
        $data['token'] = $request->input('token');
        $data['email'] = $request->input('email');

        if ($validator->fails()) {
            $data['status'] = 0;
        }else{
            $row = Mpwdresets::where('email', $request->input('email'))->where('token', $request->input('token'))->first();
            if($row){
                $expirationTime = strtotime($row->created_at) + config('auth.password.expire')*60;
                if($expirationTime < time()){
                    $data['status'] = 1;
                    Mpwdresets::where('email', $request->input('email'))->delete();
                }else{
                    $data['status'] = 2;
                }
            }else{
                $data['status'] = 1;
            }
        }
        return view("resetpwd", $data);
    }

    public function postResetpwd(Request $request)
    {
        $messages = [
            'password.confirmed' => '两次输入的密码不一致',
            'password.between' => '密码必须是6到16位',
        ];
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|between:6,16',
        ], $messages);

        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $row = Member::where('email', $credentials['email'])->first();
            $row->memberPwd = Hash::make($credentials['password']);
            $row->save();
            Mpwdresets::where('email', $credentials['email'])->delete();
            return redirect('member/login');
        }

        return redirect()->back()->with('message', '修改失败');
    }
}
