<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\News;
use Illuminate\Pagination\Paginator;

class newsController extends Controller
{
    public function __construct()
    {
        
    }

    //所有新闻
    public function getIndex()
    {
        return view('newsDetail', $data);
    }

    //新闻分类列表
    public function getType($type)
    {
        if($type == 2){
            $data['result'] = News::where('type', $type)->paginate(1);
        }else{
            $data['result'] = News::where('type', $type)->get();
        }
        return view('newsList'.$type, $data);
    }

    public function getShow($id)
    {
        $data['row'] = News::find($id);
        return view('newsDetail', $data);
    }
}
