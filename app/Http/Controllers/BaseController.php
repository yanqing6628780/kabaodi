<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class BaseController extends Controller
{
    public function __construct()
    {
        
    }

    protected function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), config('app.key'));
    }

    protected function upload_file($file_name, $request)
    {
        $dirname = property_exists($this, 'dirname') ? $this->dirname."/" : '';
        $file_path = "";
        $allow_type = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];

        if( $request->hasFile($file_name) ){
            $file = $request->file($file_name);

            $clientName = $file->getClientOriginalName();
            $entension = $file->getClientOriginalExtension();
            if( in_array($entension, $allow_type) ){
                $newName = md5(date('ymdhis').$clientName).".".$entension;
                $destinationPath = public_path().'/uploads/'.$dirname;
                $file_path = $file->move($destinationPath, $newName) ? '/uploads/'.$dirname.$newName : "";
            }else{
                $this->error = '非图片文件!';
            }

        }
        return $file_path;
    }

    public function resizeImage($file, $targetWidth, $targetHeight)
    {
        $file_path = public_path().$file;
        
        $targetRatio = $targetWidth / $targetHeight;

        $sourceWidth = Image::make($file_path)->width(); // Soruce Width
        $sourceHeight = Image::make($file_path)->height(); // Soruce Height
        $sourceRatio = $sourceWidth / $sourceHeight;

        if ( $sourceRatio < $targetRatio ) {
            $scale = $sourceWidth / $targetWidth;
        } else {
            $scale = $sourceHeight / $targetHeight;
        }

        $resizeWidth = (int)($sourceWidth / $scale);
        $resizeHeight = (int)($sourceHeight / $scale);

        $cropLeft = (int)(($resizeWidth - $targetWidth) / 2);
        $cropTop = (int)(($resizeHeight - $targetHeight) / 2);

        Image::make($file_path)->resize($resizeWidth, $resizeHeight)->crop($targetWidth, $targetHeight, $cropLeft, $cropTop)->save($file_path);
    }
}