<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Pagination\Paginator;

class productController extends Controller
{
    public function __construct()
    {
        
    }

    //产品页
    public function getIndex()
    {
        $data['category'] = Category::where('status', "Enable")->get();
        $data['result'] = Product::whereIn('cateId', $data['category']->lists('cateId'))->where('status', 'Enable')->paginate(9);
        return view('productIndex', $data);
    }

    //产品页
    public function getCategory($cateId)
    {
        $data['category'] = Category::where('status', "Enable")->get();
        $data['result'] = Product::where('cateId', $cateId)->where('status', "Enable")->paginate(9);
        return view('productIndex', $data);
    }

    public function getShow($id)
    {
        $data['row'] = Product::find($id);
        return view('productDetail', $data);
    }
}
