<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Event::listen("illuminate.query", function($query, $bindings){
//     var_dump($query);//sql 预处理 语句
//     var_dump($bindings);// 替换数据
//     echo "</br>";
// });

Route::get('/memberProfile', function () {
    return view('memberProfile');
});
Route::get('/home', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/customization', function () {
    return view('customization');
});

Route::controller('member', 'memberController');
Route::controller('news', 'newsController');
Route::controller('product', 'productController');

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/', function () {
    // return view('uc');
    return view('home');
});

