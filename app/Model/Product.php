<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Product extends Model {

    protected $table = 'product';
    protected $primaryKey = 'productId';
    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'cateId');
    }

    public function getGalleryAttribute($value)
    {
        return preg_split('/,/', $value, -1, PREG_SPLIT_NO_EMPTY);
    }

    public function setGalleryAttribute($photos)
    {
        $this->attributes['gallery'] = implode(',', $photos);
        foreach ($photos as $key => $photo) {
            $this->resizeImage($photo,640,640);
        }
    }

    public function setProductImgAttribute($file)
    {
        $this->attributes['product_img'] = $file;
        $this->resizeImage($file,640,640);
    }

    protected function resizeImage($file, $targetWidth, $targetHeight)
    {
        $file_path = public_path($file);
        
        $targetRatio = $targetWidth / $targetHeight;

        $sourceWidth = Image::make($file_path)->width(); // Soruce Width
        $sourceHeight = Image::make($file_path)->height(); // Soruce Height
        $sourceRatio = $sourceWidth / $sourceHeight;

        if ( $sourceRatio < $targetRatio ) {
            $scale = $sourceWidth / $targetWidth;
        } else {
            $scale = $sourceHeight / $targetHeight;
        }

        $resizeWidth = (int)($sourceWidth / $scale);
        $resizeHeight = (int)($sourceHeight / $scale);

        $cropLeft = (int)(($resizeWidth - $targetWidth) / 2);
        $cropTop = (int)(($resizeHeight - $targetHeight) / 2);

        Image::make($file_path)->resize($resizeWidth, $resizeHeight)->crop($targetWidth, $targetHeight, $cropLeft, $cropTop)->save($file_path);
    }
}