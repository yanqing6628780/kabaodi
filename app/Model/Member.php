<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    protected $table = 'member';
    protected $primaryKey = 'memberId';
    public $timestamps = true;

    public function aftersale()
    {
        return $this->hasMany('App\Model\Aftersale', 'memberId');
    }

}