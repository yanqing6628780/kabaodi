<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Aftersale extends Model {

    protected $table = 'aftersale';
    protected $primaryKey = 'asId';
    public $timestamps = true;

    public function member()
    {
        return $this->belongsTo('App\Model\Member', 'memberId');
    }

}