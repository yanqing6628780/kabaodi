<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Memberpwd_resets extends Model {

    protected $table = 'memberpwd_resets';
    protected $guarded = [];
}