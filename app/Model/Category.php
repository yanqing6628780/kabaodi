<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'category';
    protected $primaryKey = 'cateId';
    public $timestamps = true;

    public function product()
    {
        return $this->hasMany('App\Model\Product', 'cateId');
    }

}