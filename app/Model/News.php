<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class News extends Model {

    protected $table = 'news';
    protected $guarded = ['id'];
    public $timestamps = true;

    public function setImgAttribute($file)
    {
        $this->attributes['img'] = $file;
        $this->resizeImage($file,668,360);
    }

    protected function resizeImage($file, $targetWidth, $targetHeight)
    {
        $file_path = public_path($file);
        
        $targetRatio = $targetWidth / $targetHeight;

        $sourceWidth = Image::make($file_path)->width(); // Soruce Width
        $sourceHeight = Image::make($file_path)->height(); // Soruce Height
        $sourceRatio = $sourceWidth / $sourceHeight;

        if ( $sourceRatio < $targetRatio ) {
            $scale = $sourceWidth / $targetWidth;
        } else {
            $scale = $sourceHeight / $targetHeight;
        }

        $resizeWidth = (int)($sourceWidth / $scale);
        $resizeHeight = (int)($sourceHeight / $scale);

        $cropLeft = (int)(($resizeWidth - $targetWidth) / 2);
        $cropTop = (int)(($resizeHeight - $targetHeight) / 2);

        Image::make($file_path)->resize($resizeWidth, $resizeHeight)->crop($targetWidth, $targetHeight, $cropLeft, $cropTop)->save($file_path);
    }
}