<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model('App\Model\News')->title('新闻')->display(function ()
{
    $display = AdminDisplay::datatablesAsync();
    $display->columnFilters([
        ColumnFilter::select()->options(['1' => '新闻活动', '2' => '行业资讯'])->filter_field('type'),
    ]);
    $display->columns([
        Column::image('img')->label('缩略图'),
        Column::string('title')->label('标题'),
        Column::custom()->label('所属栏目')->callback(function ($instance) {
            return $instance->type==1 ? '新闻活动' : '行业资讯';
        }),
    ]);
    return $display;
})->createAndEdit(function ()
{
    $form = AdminForm::form();
    $form->items([
        FormItem::select('type', '所属类别')->options([1 => '新闻活动', 2 => '行业资讯'])->required(),
        FormItem::text('title', '标题')->required(),
        FormItem::image('img', '产品图')->required(),
        FormItem::textarea('desc', '摘要'),
        FormItem::ckeditor('content', '内容'),
    ]);
    return $form;
});