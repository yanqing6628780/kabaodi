<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model('App\Model\Member')->title('会员管理')->display(function ()
{
    $display = AdminDisplay::datatablesAsync();
    $display->columnFilters([
        ColumnFilter::select()->options(['Enable' => 'Enable', 'Disable' => 'Disable'])->filter_field('status'),
    ]);
    $display->columns([
        Column::image('profile')->label('头像'),
        Column::string('memberName')->label('会员名'),
        Column::string('mobile')->label('手机'),
        Column::string('email')->label('电邮'),
        Column::string('name')->label('真实姓名'),
        Column::string('status')->label('状态'),
    ]);
    return $display;
})->edit(function ()
{
    $form = AdminForm::form();
    $form->items([
        FormItem::select('status', '状态')->enum(['Enable', 'Disable'])->defaultValue('Enable')->required(),
        FormItem::image('profile', '头像'),
        FormItem::text('mobile', '手机')->required(),
        FormItem::text('email', '电邮')->required(),
        FormItem::text('name', '真实姓名'),
    ]);
    return $form;
});