<?php

Admin::menu('App\Model\Member')->label('会员管理');
Admin::menu('App\Model\Category')->label('类别管理');
Admin::menu('App\Model\Product')->label('产品管理');
Admin::menu('App\Model\News')->label('新闻管理');
Admin::menu('App\Model\Aftersale')->label('售后管理');
