<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model('App\Model\Product')->title('产品')->display(function ()
{
    $display = AdminDisplay::datatablesAsync();
    $display->columnFilters([
        ColumnFilter::select()->model('App\Model\Category')->display('cateName')->filter_field('cateId'),
    ]);
    $display->columns([
        Column::image('product_img')->label('产品图'),
        Column::string('productName')->label('产品名称'),
        Column::string('category.cateName')->label('所属类别'),
        Column::string('status')->label('状态'),
    ]);
    return $display;
})->createAndEdit(function ()
{
    $form = AdminForm::form();
    $form->items([
        FormItem::select('cateId', '所属类别')->model('App\Model\Category')->display('cateName')->required(),
        FormItem::text('productName', '产品名称')->required(),
        FormItem::image('product_img', '产品图')->required(),
        FormItem::images('gallery', '产品相册')->required(),
        FormItem::ckeditor('productDesc', '产品描述'),
        FormItem::select('status', '状态')->enum(['Enable', 'Disable'])->defaultValue('Enable')->required(),
    ]);
    return $form;
});