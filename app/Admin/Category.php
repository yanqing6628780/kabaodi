<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model('App\Model\Category')->title('类别')->display(function ()
{
    $display = AdminDisplay::datatablesAsync();
    $display->filters([
        Filter::field('status'),
    ]);
    $display->columnFilters([
        ColumnFilter::select()->options(['Enable' => 'Enable', 'Disable' => 'Disable'])->filter_field('status'),
    ]);
    $display->columns([
        Column::string('cateName')->label('名称'),
        Column::string('status')->label('状态')->append(Column::filter('status')),
    ]);
    return $display;
})->createAndEdit(function ()
{
    $form = AdminForm::form();
    $form->items([
        FormItem::text('cateName', '名称')->required(),
        FormItem::select('status', '状态')->enum(['Enable', 'Disable'])->required(),
    ]);
    return $form;
});