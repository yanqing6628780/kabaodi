<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model('App\Model\Aftersale')->title('售后')->display(function ()
{
    $display = AdminDisplay::datatablesAsync();
    $display->columnFilters([
        ColumnFilter::select()->options(['0' => '未处理', '1' => '已处理'])->filter_field('status'),
    ]);
    $display->columns([
        Column::string('member.memberName')->label('会员')->orderable(false),
        Column::string('member.mobile')->label('会员手机')->orderable(false),
        Column::string('prove')->label('商品订单信息'),
        Column::string('reason')->label('申请售后原因'),
        Column::string('asDesc')->label('申请售后说明'),
        Column::image('asImg')->label('凭证'),
        Column::custom()->label('状态')->callback(function ($instance)
        {
            return $instance->status ? '已处理' : '未处理';
        }),
    ]);
    return $display;
})->edit(function ()
{
    $form = AdminForm::form();
    $form->items([
        FormItem::text('prove', '商品订单信息')->required(),
        FormItem::text('reason', '申请售后原因')->required(),
        FormItem::text('asDesc', '申请售后说明')->required(),
        FormItem::image('asImg', '凭证')->required(),
        FormItem::select('status', '状态')->options([0 => '未处理', 1 => '已处理'])->required(),
    ]);
    return $form;
});