@extends('layout.default')

@section('content')
<div class="memberEnter">
    @if ($status == 0)
    <h3 class="page-title hidden-xs">非法请求</h3>
    @elseif ($status == 1) 
    <h3 class="page-title hidden-xs">链接失效</h3>
    @else
    <h3 class="page-title hidden-xs">密码重设</h3>
    @endif
    <div class="container-fluid login-form">
        <div class="row">
            @if ($status == 0)
            <h3 class="page-title visible-xs-block">非法请求</h3>
            @elseif ($status == 1) 
            <h3 class="page-title visible-xs-block">链接失效</h3>
            @else
            <h3 class="page-title visible-xs-block">密码重设</h3>
            <form id="formLogin" class="form-horizontal" method="post" action="">
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label" >新密码:</label>
                    <div class="col-md-4 ">
                        <input type="password" class="form-control" name="password" placeholder="请输入新密码" aria-describedby="helpBlock" required="true">
                        <span id="helpBlock" class="help-block text-small">
                            @if (count($errors->get('password')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('password')) !!}
                            </span>                            
                            @endif
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label" >确认密码:</label>
                    <div class="col-md-4 ">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="请输入确认密码" aria-describedby="helpBlock" required="true">
                     </div>
                </div>
                <div class="form-group" >
                    <div class="col-md-offset-5 col-md-2 " >
                        <input type="hidden" name="token" value="{{$token}}">
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default btn-block btn-login">提交</button>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
$(function () {
    @if (session('message'))
    alert('{{session("message")}}');
    @endif
})
</script>
@endsection