@extends('layout.default')

@section('content')
<div class="topic-img">
    <a href="#"><img class="img-responsive" src="/assets/img/about/banner.jpg" alt="图片加载失败" height="100%" /></a>
</div>
<!-- content -->
<div class="ct-about">
    <div class="companyProfile">
        <div class="media">
            <img class="media-object img-responsive col-sm-4" src="/assets/img/about/kbd.jpg" alt="卡宝地" style="margin:0 auto;">
            <div class=" col-sm-8" id="aboutfont">
                <h4 ><em>INTRODUCTION</em><br/>公司简介</h4>
                <p>1991 年， 卡宝地珠宝定制创始人之一投身珠宝设计和制造， 二十多年的钻研， 呈献了数不胜数的独特珠宝作品， 在珠宝发展史上写下美丽的篇章. 卡宝地珠宝将高端私人定制服务带到顾客面前， 倾听顾客的故事， 为顾客倾心打 造有灵魂的珠宝。珍稀的宝石， 经过专属的设计， 融聚精湛的工艺， 赋予装饰、 财富、 情感价值， 成为唯一的珍品。同时， 卡宝地珠宝将打造主题专属系列产品 出售， 满足各顾客需求。</p>
            </div>
        </div>
    </div>
    <ul class="yellowline">
        <li></li>
        <li></li>
    </ul>
    <img src="/assets/img/about/brand.jpg" alt="图片加载失败" class="iminsertion img-responsive" width="100%" />
    <ul class="yellowline setput">
        <li></li>
        <li class="triangle">
            <i>
            <em></em>
            <span></span>
            </i>
        </li>
    </ul>
    <div class="definition">
        <h2>DEFINITION<br/><span>品牌诠释</span></h2>
        <br/>
        <div>
            <img src="/assets/img/about/bluezhubao.jpg" alt="图片加载失败" class="img-responsive col-sm-7 hidden-xs navbar-left"/>
            <img src="/assets/img/about/bluezhubao.jpg" alt="图片加载失败" class="img-responsive visible-xs-block"/>
            <div class="col-sm-5 hidden-xs " id="aboutfont">
                <p>散发着优雅气息的欧洲大陆上， 高级珠宝定制早已盛行</p>
                <span class="aboutline"></span>
                <p style="color:#d1ab6d;">经过<strong>倾心</strong>设计成为永恒印记<br/>
                经过<strong>精心</strong>雕琢成为稀世杰作</p>
                <span class="aboutline"></span>
                <div>记录着主人的动人故事</div>
                <div>象征着主人的身份地位</div>
            </div>
            <div class=" visible-xs-block" id="aboutfont">
                <p>散发着优雅气息的欧洲大陆上， 高级珠宝定制早已盛行</p>
                <span class="aboutline"></span>
                <p style="color:#d1ab6d;">经过<strong>倾心</strong>设计成为永恒印记<br/>
                经过<strong>精心</strong>雕琢成为稀世杰作</p>
                <span class="aboutline"></span>
                <div>记录着主人的动人故事</div>
                <div>象征着主人的身份地位</div>
            </div>
        </div>
        <!-- part02 -->
        <div class="col-sm-12 hidden-xs" style="margin-top:30px;" >
            <div class="hidden-xs col-sm-5 navbar-left" style="margin-top:30px;" id="aboutfont">
                <center>
                <span class="aboutline"></span>
                <h3>凝聚了自然和工艺之美</h3>
                <span class="aboutline"></span>
                <p>是浪漫故事的印记</p>
                <p>是彰显才华的结晶</p>
                <p>是表达个性的宣言</p>
                </center>
            </div>
            <img src="/assets/img/about/content02.jpg" alt="加载失败" width="100%" class="col-sm-7 navbar-right"/>
        </div>
        <div class=" visible-xs-block" style="margin-top:30px;">
            <img src="/assets/img/about/content02.jpg" alt="加载失败" width="100%"/>
            <div id="aboutfont">
                <center>
                <span class="aboutline"></span>
                <h3>凝聚了自然和工艺之美</h3>
                <span class="aboutline"></span>
                <p>是浪漫故事的印记</p>
                <p>是彰显才华的结晶</p>
                <p>是表达个性的宣言</p>
                </center>
            </div>
        </div>
        <div class="con03">
            <p>每一款珠宝都演绎着一段非凡的故事
            <br/> 经由时间的沉淀， 永恒传承， 万代流传</p>
        </div>
        <div class="container-fluid" style="padding: 0;">
            <img src="/assets/img/about/content03.jpg" alt="图片加载失败" title="贵族之美" width="100%" />
        </div>
    </div>
    <!-- content -->
</div>
@endsection