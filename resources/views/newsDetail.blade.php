@extends('layout.default')

@section('content')
<div class="topic-img">
    <a href="#"><img class="img-responsive" src="/assets/img/news/industry-banner.jpg" alt="图片加载失败" height="100%" /></a>
</div>
<!-- content -->
<div class="ct-action">
    <div class="industryCenter">
        <h2><span>{{$row->title}}</span></h2>
        <ul class="yellowline setput">
            <li></li>
            <li ></li>
        </ul>
        <div>{{ date('Y.m.d', strtotime($row->created_at)) }}<a href="javascript:history.go(-1);" style="float:right">返回</a></div>
        <div class="con01 industryCon01 newsimg">
            <p>{!! $row->content !!} </p>
        </div>
    </div>
</div>
@endsection