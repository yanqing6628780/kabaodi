@extends('layout.default')

@section('content')
<!-- banner -->
<a href="/" ><img src="/assets/img/design/banner.jpg" alt="加载失败" class="img-responsive" /></a>
<!-- serveBD02Start -->
<div class="ct-about">
    <h2 class=" titleline custitle">珠宝高级定制</h2>
    <span class="aboutline"></span>
    <span class="aboutline"></span>
    <div class="severdefinition">
        <!-- id="cusline"-->
        <img src="/assets/img/design/bgline.png" alt="图片加在失败" style="position:absolute;" class="img-responsive hidden-xs" id="custiom-line"/>
        <h2>CUSTOMIZATION PROCESS<br/><span>卡宝地定制流程</span></h2>
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb07.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 push-left" />
            <img src="/assets/img/design/zb07.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs" id="customfont">
                <em>CREATIVE</em>
                <h3>专属定制</h3>
                <p style="text-indent:2em;display:block">
                卡宝地的设计师将根据您的故事作为灵感来源，
                耐心与您沟通、细化修改，只愿为您设计仅属于您
                的独一无二的珠宝首饰。
                </p>
            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>CREATIVE</em>
                <h3>专属定制</h3>
                <p style="text-indent:2em;display:block">
                卡宝地的设计师将根据您的故事作为灵感来源，
                耐心与您沟通、细化修改，只愿为您设计仅属于您
                的独一无二的珠宝首饰。
                </p>
            </div>
        </div>
        <!-- <div class="con01 clearFloat"> -->
        <!-- part02 -->
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb06.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 navbar-right" style="margin-right:0" />
            <img src="/assets/img/design/zb06.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%" />

            <div class="col-sm-6 cusclass hidden-xs navbar-left" id="customfont">
                <em>SELECT</em>
                <h3>甄选宝石</h3>
                <p style="text-indent:2em;display:block">
                根据设计，为您的珠宝搭配形状、颜色、切割
                最优质的宝石，每颗宝石都经过专业人员精心挑选，
                精选您喜欢的优质配材衬托。
                </p>
            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>SELECT</em>
                <h3>甄选宝石</h3>
                <p style="text-indent:2em;display:block">
                根据设计，为您的珠宝搭配形状、颜色、切割
                最优质的宝石，每颗宝石都经过专业人员精心挑选，
                精选您喜欢的优质配材衬托。
                </p>
            </div>
        </div>
        <!-- part03 -->
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb05.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 push-left" />
            <img src="/assets/img/design/zb05.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs" id="customfont">
                <em>WAX CARVING</em>
                <h3>模型造版</h3>
                <p style="text-indent:2em;display:block">
                设计手稿完成后，卡宝地的工匠们将结合传统
                手艺与现代科技，跨越一道道技术挑战，雕刻精准
                的一比一珠宝蜡版。这对珠宝工匠们的专业度、精
                准度、美感都有很高的要求。
                </p>
                <center>
                <span class="aboutline"></span>
                <h3 style="color:#d1ab6d">凝聚了自然和工艺之美</h3>
                <span class="aboutline"></span>
                </center>
            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>WAX CARVING</em>
                <h3>模型造版</h3>
                <p style="text-indent:2em;display:block">
                设计手稿完成后，卡宝地的工匠们将结合传统
                手艺与现代科技，跨越一道道技术挑战，雕刻精准
                的一比一珠宝蜡版。这对珠宝工匠们的专业度、精
                准度、美感都有很高的要求。
                </p>
                <center>
                <span class="aboutline"></span>
                <h3 style="color:#d1ab6d">凝聚了自然和工艺之美</h3>
                <span class="aboutline"></span>
                </center>
            </div>
        </div>
        <!-- <div class="con01 clearFloat"> -->
        <!-- part04 -->
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb04.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 navbar-right" style="margin-right:0" />
            <img src="/assets/img/design/zb04.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs navbar-left" id="customfont">
                <em>CASTING</em>
                <h3>倒模</h3>
                <p style="text-indent:2em;display:block">
                工匠们将制作完成的蜡版种入蜡树中，进入贵
                金属打造阶段。在高温熔炉中精密铸造珠宝雏形。
                </p>
            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>CASTING</em>
                <h3>倒模</h3>
                <p style="text-indent:2em;display:block">
                工匠们将制作完成的蜡版种入蜡树中，进入贵
                金属打造阶段。在高温熔炉中精密铸造珠宝雏形。
                </p>
            </div>
        </div>
        <!-- part05-->
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb03.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 push-left" />
            <img src="/assets/img/design/zb03.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs" id="customfont">
                <em>JEWELRY FABRICATION</em>
                <h3>执模</h3>
                <p style="text-indent:2em;display:block">
                这阶段工匠们需要完成一系列的完善工作，修
                饰、修补工件缺陷，对工件进行整形、焊接、初步
                打磨铸件表面、装配珠宝配件等等。
                </p>

            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>JEWELRY FABRICATION</em>
                <h3>执模</h3>
                <p style="text-indent:2em;display:block">
                这阶段工匠们需要完成一系列的完善工作，修
                饰、修补工件缺陷，对工件进行整形、焊接、初步
                打磨铸件表面、装配珠宝配件等等。
                </p>
            </div>
        </div>

        <!-- part06-->
        <div class="col-sm-12" style="margin-top:20px">
            <img src="/assets/img/design/zb02.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 navbar-right" style="margin-right:0"/>
            <img src="/assets/img/design/zb02.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs" id="customfont">
                <em>STONE SETTING & POLISHING</em>
                <h3>镶嵌与打磨</h3>
                <p style="text-indent:2em;display:block">
                珠宝雏形做好后需镶上宝石，这过程还需珠宝
                工匠们一点点地对珠宝进行精细打磨、抛光。最后
                通过对珠宝进行电金工艺处理，使其更晶莹璀璨。
                </p>

            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>STONE SETTING & POLISHING</em>
                <h3>镶嵌与打磨</h3>
                <p style="text-indent:2em;display:block">
                珠宝雏形做好后需镶上宝石，这过程还需珠宝
                工匠们一点点地对珠宝进行精细打磨、抛光。最后
                通过对珠宝进行电金工艺处理，使其更晶莹璀璨。
                </p>
            </div>
        </div>
        <!-- <div class="con01 clearFloat"> -->
        <!-- part07 -->
        <div class="col-sm-12" style="margin-top:20px;margin-bottom:20px;">
            <img src="/assets/img/design/zb.png" alt="加载失败"  class="img-responsive hidden-xs  col-sm-6 navbar-left" style="margin-right:0" />
            <img src="/assets/img/design/zb.png" alt="加载失败"  class="img-responsive visible-xs-block" width="100%"/>

            <div class="col-sm-6 cusclass hidden-xs navbar-right" id="customfont" style="margin-right:0px">
                <em>FINISH</em>
                <h3>珠宝完成</h3>
                <p style="text-indent:2em;display:block;">
                卡宝地有专业的质检部对每一步工艺进行严格
                把关，每一件珠宝成品必须通过质检部的总体质量
                检测，确保您的珠宝臻至完美。凝聚着设计师和工
                匠们心血、专属于您的珠宝终于完成了。相信卡宝
                地为您打造的珠宝一定能描绘您心中那段私属的故
                事。
                </p>
            </div>
            <div class="cusclass visible-xs-block" id="customfont">
                <em>FINISH</em>
                <h3>珠宝完成</h3>
                <p style="text-indent:2em;display:block">
                卡宝地有专业的质检部对每一步工艺进行严格
                把关，每一件珠宝成品必须通过质检部的总体质量
                检测，确保您的珠宝臻至完美。凝聚着设计师和工
                匠们心血、专属于您的珠宝终于完成了。相信卡宝
                地为您打造的珠宝一定能描绘您心中那段私属的故
                事。
                </p>
            </div>
        </div>
        <div class="text-center font-botton " style="position:relative; z-index:2">
            每一款珠宝都演绎着一段非凡的故事<br/>
            经由时间的沉淀， 永恒传承， 万代流传
        </div>
    </div>
</div>
@endsection