@extends('layout.default')

@section('content')
<div class="container-fluid ct-member">
    <h3 class="memberCenterFont">会员中心</h3>
    <div class="companyProfile industryHeight memberCenter">
        <ol>
            <li><a href="#" id="member"><i class="iconfont visible-xs-inline"  id="huiyuanziliao">&#xe697;</i>会员资料</a></li>
            <li><a href="#" id="sale"><i class="  visible-xs-inline" id="shouhuofuwu"></i>售后服务</a></li>
        </ol>
    </div>
    <ul class="yellowline setput memberMove">
        <li></li>
        <li class="triangle">
            <i class="huiyuansanjiao hidden-xs">
            <em></em>
            <span></span>
            </i>
            <i class="seversanjiao hidden-xs">
            <em></em>
            <span></span>
            </i>
        </li>
    </ul>
    <div class="personalTable" id="userMember">
        <div class="headline">基本资料</div>
        <form enctype="multipart/form-data" action="{{url('member/profile')}}" method="post">
            <div class="row">
                <div class="col-sm-8">
                    <table class="table table-hover memberinfo">
                        <tbody>
                            <tr>
                                <th>用户名:</th>
                                <td>{{$member->memberName}}</td>
                            </tr>
                            <tr id="font01">
                                <th>密码:</th>
                                <td ><span class="cot01">*******</span><input type="text" name="password" class="one form-control" style="display:none;margin-top:10px;" /></td>
                                <td><a onclick="modificationDate('.one','.cot01','.onetext')" class="onetext">修改</a></td>
                            </tr>
                            <tr id="font02">
                                <th>手机:</th>
                                <td ><span class="cot02">{{$member->mobile}}</span><input type="text" name="mobile" class="two form-control" style="display:none;margin-top:10px;" value="{{$member->mobile}}" /></td>
                                <td><a onclick="modificationDate('.two','.cot02','.twotext')" class="twotext">修改</a></td>
                            </tr>
                            <tr id="font03">
                                <th>邮箱:</th>
                                <td ><span class="cot03">{{$member->email}}</span><input type="text" name="email" class="three form-control" style="display:none;margin-top:10px;" value="{{$member->email}}" /></td>
                                <td><a onclick="modificationDate('.three','.cot03','.threetext')" class="threetext">修改</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-offset-1 col-sm-3">
                    <div class="ds">
                        <img id="imghead" src="{{ $member->profile or 'assets/img/register/logo.jpg'}}" alt="{{$member->memberName}}" width="100%"  class="img-responsive" />
                        <!--  -->
                        <div class="file-box">
                            <input type='button' class='btn ' value='上传' />
                            <input type="file" name="profile" class="file" id="fileField" size="28" onchange="previewImage(this)" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="margin-top: 5%;">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                <input type="submit" value="提交修改" class="handDate"/>
            </div>
        </form>
    </div>
    <!-- content -->
    <!-- content02 -->
    <div class="personalTable" id="userSale">
        <form enctype="multipart/form-data" action="{{url('member/aftersale')}}" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="resion">
                        <label for="prove" class="col-md-2 control-label col-md-offset-2 text-right">商品订单信息:</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="prove" name="prove" placeholder="输入订单号/产品序列号/条形码" value="{{old('prove')}}">
                        </div>
                    </div>
                    <div class="form-group" id="resion">
                        <label for="reason" class="col-md-2 control-label col-md-offset-2 text-right" >申请售后原因:</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="reason" name="reason" placeholder="需要维护" value="{{old('email')}}">

                        </div>
                    </div>
                    <div class="form-group" id="resion">
                        <label for="asDesc" class="col-md-2 control-label col-md-offset-2 text-right">申请售后说明:</label>
                        <div class="col-md-4">
                            <textarea name="asDesc" id="asDesc" cols="100%" rows="10%" class="form-control">{{old('asDesc')}}</textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group" id="resion" >
                        <label for="prove" class="col-md-2 control-label col-md-offset-2 text-right">上传凭证:</label>
                        <div class="col-md-4">
                            <input type='button' class='btn' value='上传' />
                            <input type="file" name="asImg" class="file" id="fileField" size="28" onchange="document.getElementById('textfield').value=this.value" accept="image/png,image/gif,image/jpg,image/jpeg" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="margin-top: 5%;">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                <input type="submit" value="提交修改" class="handDate" />
            </div>
        </form>
    </div>
</div>

@endsection

@section('page_js')
<script type="text/javascript">
$(function () {
    @if (session('message'))
    alert('{{session("message")}}');
    @endif
});
function previewImage(file) {
    var MAXWIDTH = 260;
    var MAXHEIGHT = 180;
    if (file.files && file.files[0]) {
        var img = document.getElementById('imghead');
        img.onload = function() {
            var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
            img.style.marginTop = rect.top + 'px';
        }
        var reader = new FileReader();
        reader.onload = function(evt) {
            img.src = evt.target.result;
        }
        reader.readAsDataURL(file.files[0]);
    } else { //兼容IE
        var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
        file.select();
        var src = document.selection.createRange().text;
        div.innerHTML = '<img id=imghead>';
        var img = document.getElementById('imghead');
        img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
        var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
        status = ('rect:' + rect.top + ',' + rect.left + ',' + rect.width + ',' + rect.height);
        div.innerHTML = "<div id=divhead style='width:" + rect.width + "px;height:" + rect.height + "px;margin-top:" + rect.top + "px;" + sFilter + src + "\"'></div>";
    }
}
function clacImgZoomParam( maxWidth, maxHeight, width, height )
{
   var param = {top:0, left:0, width:width, height:height};
   if( width>maxWidth || height>maxHeight )
   {
       rateWidth = width / maxWidth;
       rateHeight = height / maxHeight;
        
       if( rateWidth > rateHeight )
       {
           param.width =  maxWidth;
           param.height = Math.round(height / rateWidth);
       }else
       {
           param.width = Math.round(width / rateHeight);
           param.height = maxHeight;
       }
   }
    
   param.left = Math.round((maxWidth - param.width) / 2);
   param.top = Math.round((maxHeight - param.height) / 2);
   return param;
}
</script>
@endsection