<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="卡宝地珠宝有限公司,卡宝地珠宝，珠宝,卡宝地"/>
    <title>卡宝地珠宝有限公司</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="assets/css/public.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/header.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/content.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/copyright.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/layoutnew.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		
		<!--首页中部-->
		<section id="">
					<img src="assets/uc.jpg" class="img-responsive">
		</section>
		<!--首页中部 end-->

    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
	
  </body>
</html>
