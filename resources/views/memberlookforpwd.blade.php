@extends('layout.default')

@section('content')
<div class="memberEnter">
    <h3 class="page-title hidden-xs">找回密码</h3>
    <div class="container-fluid login-form">
        <div class="row">
            <h3 class="page-title visible-xs-block">找回密码</h3>
            <form id="formLogin" class="form-horizontal" method="post" action="">
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label" >请输入电子邮箱:</label>
                    <div class="col-md-4 ">
                        <input type="email" class="form-control" name="email" placeholder="请输入邮箱" aria-describedby="helpBlock" required="true">
                        <span id="helpBlock" class="help-block text-small">
                            @if (count($errors->get('email')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('email')) !!}
                            </span>
                            @else
                            请输入有效邮箱
                            @endif
                        </span>
                    </div>
                </div>
                <div class="form-group" >
                    <div class="col-md-offset-5 col-md-2 " >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default btn-block btn-login">发送</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
$(function () {
    @if (session('message'))
    alert('{{session("message")}}');
    @endif
})
</script>
@endsection