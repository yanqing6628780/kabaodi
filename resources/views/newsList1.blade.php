@extends('layout.default')

@section('content')
<div class="topic-img">
    <a href="#"><img src="/assets/img/news/journalistic-banner.jpg" alt="图片加载失败" class="img-responsive" width="100%" /></a>
</div>
<div class="ct-activity">
    <!-- content -->
    <div class="companyProfile industryHeight">
        <h3>新闻活动</h3>
    </div>
    <ul class="yellowline setput">
        <li></li>
        <li class="triangle">
            <i>
            <em></em>
            <span></span>
            </i>
        </li>
    </ul>
    <div class="container-fluid">
        <div class="row">
            @foreach ($result as $item)
            <div class="col-md-6 info-item">
                <img class="img-responsive" src="/{{$item->img}}" alt="{{$item->title}}" width="100%"/>
                <dl>
                    <dt>{{$item->title}}</dt>
                    <dd style="min-height: 38px">{{$item->desc}}</dd>
                    <a href="{{url('news/show/'.$item->id)}}">
                        <dd class="journalisticMetion">了解详情</dd>
                    </a>
                </dl>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection