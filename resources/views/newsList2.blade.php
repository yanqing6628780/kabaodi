@extends('layout.default')

@section('content')
<div class="ct-action">
    <div class="companyProfile industryHeight">
        <h3>行业资讯</h3>
    </div>
    <ul class="yellowline setput">
        <li></li>
        <li class="triangle">
            <i>
            <em></em>
            <span></span>
            </i>
        </li>
    </ul>
    <div class="industryCenter">
        @foreach ($result as $item)
        <h2><span>{{$item->title}}</span></h2>
        <div class="con01 industryCon01 newsimg">
            <img src="/{{$item->img}}" alt="{{$item->title}}" width="100%" />
            <p>{{$item->desc}}</p>
            <a href="{{url('news/show/'.$item->id)}}" class="getInformation">了解详情</a>
        </div>
        @endforeach
        {!! $result->render() !!}
           
    </div>
</div>
@endsection