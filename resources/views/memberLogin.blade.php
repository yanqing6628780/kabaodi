@extends('layout.default')

@section('content')
<div class="memberEnter">
    <h3 class="page-title hidden-xs">会员登录</h3>
    <div class="container-fluid login-form">
        <div class="row">
            <div class="col-md-offset-3 col-md-2">
                <img src="/assets/img/register/logo.jpg" alt="图片加载失败" class="img-responsive" />
            </div>
            <h3 class="page-title visible-xs-block">会员登录</h3>
            <div class="col-md-5">
                <form id="formLogin" class="form-horizontal" method="post" action="">
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">会员名称</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="username" name="username" placeholder="用户名" required="true" value="{{old('username')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="userPwd" class="col-md-3 control-label" >密码</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" id="userPwd" name="userPwd" placeholder="6-16位字符" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="authCode" class="col-md-3 control-label" >验证码</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="authCode" name="captcha" placeholder="验证码" aria-describedby="helpBlock" required="true">
                            <span id="helpBlock" class="help-block text-small">输入右图中的字符，不区分大小写</span>
                        </div>
                        <div class="col-md-5">
                            <span class="verificationCode"><img id="captcha" src="{{captcha_src()}}" alt="图片加载中..." height="35" style="display: inline-block;" /><a href="javascript:getCaptcha('captcha')">看不清，换一张</a></span>
                        </div>
                    </div>
                    <div class="form-group" >
                        <div class="col-md-offset-5 col-md-5 col-sm-3 col-sm-offset-3 col-xs-5 col-xs-offset-3" id="loginbutton">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-default btn-block btn-login">登录</button>
                        </div>
                        <br />
                        <br />
                        <div class="text-center" id="loginbut-font">
                            <a href="{{url('member/lookforpwd')}}" class="forgetPws text-center" style="display:inline-block;" id="fontput">忘记密码?</a>&nbsp;
                            <a href="{{url('member/register')}}" class="forgetPws visible-xs-inline">会员注册</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
$(function () {
    @if (session('message'))
    alert('{{session("message")}}');
    @endif
})
</script>
@endsection