@extends('layout.default')

@section('content')
<div class="memberEnter registerpage" id="product">
    <h3 class="page-title visible-xs-block" style="margin-bottom:20px;font-size:15px">{{$row->productName}}</h3>
    <div class="container-fluid ">
        <div class="row" style="margin-bottom:30px;" >
            <div class="col-md-12 visible-xs-block" style="margin-bottom:10px">
                <h3 class="hidden-xs">{{$row->productName}}</h3>
                <div class="pro-line"></div>
                <div class="pro-line"></div>
                <div >
                    {!! $row->productDesc !!}
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4 hidden-xs" style="margin-bottom:100px">
                <a href="#"><img src="/{{$row->product_img}}" alt="{{$row->productName}}" width="100%" class="img-responsive picshow"  /></a>
                <ul class="productDot col-xs-offset-3 visible-xs-block">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>

            <div class="col-md-6 hidden-xs" >

                <div class="col-md-12 hidden-xs" id="pro-content">
                    <h3>{{$row->productName}}</h3>
                    <div class="pro-line"></div>
                    <div class="pro-line"></div>
                    <div class="pro-content">
                        {!! $row->productDesc !!}
                    </div>
                </div>
                <div class="pro-play col-md-12 col-md-offset-2 hidden-xs" id="chanpinchose">
                    <span><img src="/assets/img/productsconten/left.jpg" alt="图片加载失败" class="img-responsive left-but" width="100%"/></span>
                    @foreach ($row->gallery as $photo)
                    <span><img src="{{'/'.$photo}}" alt="图片加载失败" class="img-responsive pro-pic" /></span>
                    @endforeach
                    <span><img src="/assets/img/productsconten/right.jpg" alt="图片加载失败" class="img-responsive right-but" width="100%"/></span>
                </div>
            </div>
             <div class="visible-xs-block">       
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><img src="/{{$row->product_img}}" alt="{{$row->productName}}" /></div>
                        @foreach ($row->gallery as $photo)
                        <div class="swiper-slide"><img src="{{'/'.$photo}}" alt="图片加载失败" /></div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>

        </div>
        <div class="col-md-2 col-md-offset-6">
            <a href="javascript:history.go(-1);"> <button type="submit" class="btn btn-default btn-block btn-login">返回</button></a>
        </div>
    </div>
</div>
@endsection