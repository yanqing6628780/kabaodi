@extends('layout.default')

@section('content')
<div class="memberEnter registerpage">
    <h3 class="page-title hidden-xs">会员注册</h3>
    <div class="container-fluid reg-form">
        <div class="row">
            <div class="col-md-offset-2 col-md-3">
                <a href="#"><img src="/assets/img/register/logo.jpg" alt="图片加载失败" width="100%" class="img-responsive"  /></a>
            </div>
            <h3 class="page-title visible-xs-block">会员注册</h3>
            <div class="col-md-6">
                <form id="formRegister" class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label"><span>*</span>注册邮箱</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="email" name="email" placeholder="电子邮箱" typeMismatch="true" required="true" value="{{old('email')}}">
                            @if (count($errors->get('email')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('email')) !!}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label"><span>*</span>用户名</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="username" name="username" placeholder="输入6-10英文或数字"  required="true" value="{{old('username')}}">
                            @if ( count($errors->get('username')) )
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('username')) !!}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="userPwd" class="col-md-3 control-label" required="true"><span>*</span>创建密码</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" id="userPwd" name="userPwd" placeholder="输入6-16位英文或数字" required="true">
                            @if (count($errors->get('userPwd')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('userPwd')) !!}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPwd" class="col-md-3 control-label" required="true"><span>*</span>确认密码</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" id="confirmPwd" name="userPwd_confirmation" placeholder="请两次输入密码" required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-md-3 control-label" required="true"><span>*</span>手机</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="请输入手机号码" required="true" value="{{old('mobile')}}">
                            @if (count($errors->get('mobile')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('mobile')) !!}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-md-3 control-label"><span>*</span>真实姓名</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="请输入你的真实姓名" required="true" value="{{old('name')}}">
                            @if (count($errors->get('name')))
                            <span class="help-block text-small">
                                {!! implode('</br>', $errors->get('name')) !!}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="wechat" class="col-md-3 control-label">输入微信号</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="wechat" name="wechat" placeholder="请输入你的微信" value="{{old('wechat')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="authCode" class="col-md-3 control-label"><span>*</span>验证码</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="authCode" name="captcha" placeholder="验证码" aria-describedby="helpBlock" required="true">
                            <span id="helpBlock" class="help-block text-small">
                                输入右图中的字符，不区分大小写
                                @if ( count($errors->get('captcha')) )
                                    </br>
                                    {!! implode('</br>', $errors->get('captcha')) !!}
                                @endif
                            </span>
                        </div>
                        <div class="col-md-5">
                            <span class="verificationCode"><img id="captcha" src="{{captcha_src()}}" alt="图片加载失败" height="35" style="display: inline-block;" /><a href="javascript:getCaptcha('captcha')">看不清，换一张</a></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" required="true" checked> 我已经阅读并接受“服务条款”
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-5 col-md-4 col-sm-4 col-sm-offset-3 col-xs-5 col-xs-offset-3">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-default btn-block btn-login">注册</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
$(function () {
    @if (session('message'))
    alert('{{session("message")}}');
    @endif
})
</script>
@endsection