@extends('layout.default')

@section('content')
<div id="banner" style="width:100%">
    <ul class="list-unstyled">
        <li class="active">
            <a href="/"><img class="img-responsive" src="/assets/img/banner02.jpg" alt="图片加载失败"  /></a>
        </li>
        <li>
            <a href="/"><img class="img-responsive"  src="/assets/img/banner01.jpg" alt="图片加载失败"  /></a>
        </li>
    </ul>
</div>
<section id="content">
    <div class="container-fluid">
        <div class="row ct-container">
            <div class="col-sm-5 col-my-4">
                <a href="{{url('about')}}">
                <img class="img-responsive" src="/assets/img/home.png" >
                <div class="brand">
                    <p class="title-en">BRAND</p>
                    <p class="title-cn">品牌承传</p>
                </div>
                </a>
            </div>
            <div class="col-sm-7 col-my-6">
                <a href="{{url('news/type/1')}}">
                <img class="img-responsive" src="/assets/img/zhubao.png">
                <div class="newinfo">
                    <p class="title-en">NEW</p>
                    <p class="title-cn">最新资讯</p>
                </div>
                </a>
            </div>
            <div class="col-sm-5 col-my-4">
                <a href="{{url('customization')}}">
                <img class="img-responsive" src="/assets/img/gem.png">
                <div class="customize">
                    <p class="title-en">CUSTOMIZATION</p>
                    <p class="title-cn">定制服务</p>
                </div>
                </a>
            </div>
            <div class="col-sm-7 col-my-6">
                <div class="subdiv">
                    <a href="{{url('member/profile')}}">
                    <img class="img-responsive" src="/assets/img/diamo.png">
                    <div class="vip">
                        <p class="title-en">VIP</p>
                        <p class="title-cn">尊贵会员</p>
                    </div>
                    </a>
                </div>
                <div class="subdiv">
                    <a href="{{url('contact')}}">
                    <img class="img-responsive" src="/assets/img/company.png">
                    <div class="contact">
                        <p class="title-en">CONTACT</p>
                        <p class="title-cn">联系我们</p>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="mask"></div>
    </div>
</section>
@endsection