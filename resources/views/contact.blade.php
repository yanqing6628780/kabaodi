@extends('layout.default')

@section('content')
<div class="container-fluid ct-contact">
    <h3 class="text-center hidden-xs">佛山市顺德区卡宝地珠宝有限公司</h3>
    <div class="row">
        <div class="col-md-5" id="logotop">
            <img src="/assets/img/content/logo.jpg" alt="图片加载失败" width="100%" />
        </div>
        <br />
        <br />
        <h3 class="text-center visible-xs-block">佛山市顺德区卡宝地珠宝有限公司</h3>
        <div class="col-md-7">
            <div style="border:#ccc solid 1px;" id="dituContent"></div>
            <div class="row conInformation">
                <div class="col-sm-8" id="contart-font">
                    <dl>
                        <dd>邮箱：karbody@sina.com</dd>
                        <dd>地 址：伦教荔村广珠路东荔村段北9号</dd>
                        <dd>电话：0757-2362 2248</dd>
                        <dd>&nbsp;QQ：3287539963</dd>
                    </dl>
                </div>
                <div class="col-sm-4">
                    <br />
                    <br />
                    <div class="hidden-xs">扫一扫<br/>关注卡宝地珠宝<br/>微信公众平台</div>
                    <div class="visible-xs-block">扫一扫关注卡宝地珠宝微信公众平台</div>
                    <img src="/assets/img/two-dimension.jpg" alt="加载失败" width="100%" class="conMap"/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.3&services=true"></script>
<script type="text/javascript" src="/assets/js/api.js"></script>
@endsection