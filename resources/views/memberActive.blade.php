<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="sam_leung">
<!-- Bootstrap -->
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row" style="margin: 10px 0">
        <div class="col-md-offset-4 col-md-4">
        <img class="img-responsive" src="{{url('/assets/img/logo.png')}}" style="margin:0 auto">
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            请点击以下连接激活会员帐户。请点击以下按钮会员帐户:
            <br>
            <a class="btn btn-success" href="{{ url('member/active?email='.$email) }}" target="_blank">
            激活帐户
            </a>
        </div>
        <div class="col-md-offset-4 col-md-4">    
            如果点击无效，请复制下方网页地址到浏览器地址栏中打开
            <br>
            {{ url('member/active?email='.$email) }}
        </div>
    </div>
</div>
</body>
</html>