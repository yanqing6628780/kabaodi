@extends('layout.default')

@section('content')
<div id="cur-height"></div>
<div class="container-fluid visible-xs-block" id="pro-phone-nav">
    <h3 class="text-left">成品展示</h3>
    <div class="text-center phone-cur" ><a href="#">KARBODY&nbsp;定制珠宝</a></div>
    @foreach ($category as $item)
    <div class="text-center"><a href="{{url('product/category/'.$item->cateId)}}">{{$item->cateName}}</a></div>
    @endforeach
</div><!-- nov02 -->
<article class="container-fluid pro-list" style="position:relative;z-index:1">
    <div class="row">
        <div class="col-sm-2 hidden-xs cat-list">
            <ul>
                <li>
                <h3>KARBODY&nbsp;定制珠宝</h3></li>
                @foreach ($category as $item)
                <li><a href="{{url('product/category/'.$item->cateId)}}">{{$item->cateName}}</a></li>
                @endforeach
            </ul>
        </div>
        <div id="cur-height02"></div>
        <div class="showWindow"></div>
        <div class="col-sm-10 col-xs-12 item-list" id="product-pic">
            <div class="row" id="product-img">
                @foreach ($result as $item)
                <div class="col-sm-6 col-md-4 pro-item">
                    <a href="{{url('product/show/'.$item->productId)}}"><img src="/{{$item->product_img}}" alt="{{$item->product_img}}" class="img-responsive "></a>
                </div>
                @endforeach
            </div>
            <div class="tabBar">
                {!! $result->render() !!}
                <ol class="hidden">
                    <li><div class="sevleftBut"></div></li>
                    <li>
                        <ul>
                            <li class="curInter">1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                        </ul>
                    </li>
                    <li><div class="sevrightBut"></div></li>
                </ol>
            </div>
        </div>
        <div class="mask"></div>
    </div>
</article>
@endsection